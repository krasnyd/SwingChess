## Swing Chess
### Anotace
Moje ročníková práce se zaměřuje na vytvoření počítačového programu, ve kterém si mohou dva hráči zahrát šachy. Program jsem vytvářel v programovacím jazyce Java na verzi 1.8. Používám v něm nástroj Maven pro správu externích knihoven a pro snazší exportování do výsledného JAR souboru. Pro vykreslování uživatelského rozhraní používám knihovnu Swing. Dále používám knihovnu Log4j2, díky které mohu lépe zaznamenávat informace o chodu programu. V projektu mám také vytvořeno několik automatických testů, na které používám knihovnu JUnit 4, díky které si mohu otestovat některé složitější metody. Poslední knihovnou, kterou používám je Apache Commons IO, ze které používám jen metodu, která mi z názvu souboru odstraní jeho příponu. 

V programu jsou naimplementována všechna základní pravidla šachů (pokud vím, tak vše kromě pravidla braní mimochodem a rošády). V budoucnu bych chtěl program rozšířit o spoustu zajímavých vylepšení např. možnost hrát se svým protihráčem po internetu, zobrazovat historii tahů nebo možnost si přizpůsobit vhled hrací desky podle svého.  	

### Uživatelský návod
Uživatel spustí program (dvojklikem na SwingChess.jar) a otevře se mu samotné uživatelské rozhraní. V něm má možnost začít hned hrát (bílý začíná) nebo má možnost si v horní nabídce vytvořit novou hru, uložit stávající hru do souboru nebo již nějakou uloženou hru načíst. Nad samotnou hrací plochou mohou hráči vidět, jak dlouho už hrají. 
### Otázky

**Q:** Z čeho máš největší radost?

**A:** Myslím si, že se mi podařilo vytvořit hezké a minimalistické GUI, které se renderuje bez nějakých záseků a je uživatelsky přívětivé.
 
##

**Q:** Co bylo nejtěžší?

**A:** Asi nejtěžší bylo pro mě správně vymyslet algoritmy pro kontrolu šach matu nebo pro zobrazování možných políček, na které může hráč posunout figurku. 

##

**Q:** Kolik hodin času si s prací strávil?

**A:** Čas jsem si nepočítal, ale odhadoval bych ho tak na 15 hodin, když počítám i přemýšlení nad objektovým modelem atp. 
