package cz.krasnyd.swingchess.model;

import cz.krasnyd.swingchess.model.pieces.Rook;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TileTest {

    @Test
    public void playerTileCheck() {
        Game g = new Game(Player.BLACK);
        Tile t = g.getTile(0,0);
        new Rook(Player.BLACK, t);
        assertTrue(t.isPlayerTile());
        assertFalse(t.isEmptyTile());
        assertFalse(t.isEnemyTile());
    }

    @Test
    public void enemyTileCheck() {
        Game g = new Game(Player.BLACK);
        Tile t = g.getTile(0,0);
        t.setPiece(null);
        assertFalse(t.isPlayerTile());
        assertTrue(t.isEmptyTile());
        assertFalse(t.isEnemyTile());
    }

    @Test
    public void emptyTileCheck() {
        Game g = new Game(Player.BLACK);
        Tile t = g.getTile(0,0);
        new Rook(Player.WHITE, t);
        assertFalse(t.isPlayerTile());
        assertFalse(t.isEmptyTile());
        assertTrue(t.isEnemyTile());
    }
}