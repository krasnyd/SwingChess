package cz.krasnyd.swingchess;

import cz.krasnyd.swingchess.model.Game;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SaveGameEvent implements ActionListener {

    private static final Logger log = LogManager.getLogger(SaveGameEvent.class);

    private static final String FILE_EXTENSION = "dat";

    private final Game game;

    public SaveGameEvent(Game game) {
        this.game = game;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        FileDialog fd = new FileDialog((Dialog) null, "Uložit hru", FileDialog.SAVE);
        fd.setFile(game.generateSaveFileName() + "." + FILE_EXTENSION);
        fd.setVisible(true);

        String filePath = fd.getDirectory() + fd.getFile();
        if (fd.getFile() != null) {
            try (FileOutputStream f = new FileOutputStream(filePath);
                 ObjectOutputStream o = new ObjectOutputStream(f)) {

                // write object to file
                o.writeObject(game);
            } catch (IOException e) {
                log.error("Error while saving game. ", e);
                JOptionPane.showMessageDialog(null,
                        "Nepodařilo se uložit hru. Chyba: \n" + e.getClass().getName() + ": " + e.getLocalizedMessage(),
                        "Chyba",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
