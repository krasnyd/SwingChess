package cz.krasnyd.swingchess;

import cz.krasnyd.swingchess.model.Game;
import cz.krasnyd.swingchess.model.Player;
import cz.krasnyd.swingchess.model.Tile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class GameClient extends JFrame {

    private static final Logger log = LogManager.getLogger(GameClient.class);

    private static final String WINDOW_TITLE = "SwingChess";

    private static final List<GameClient> gameClients = new ArrayList<>();

    final transient ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(3);

    public static void main(String[] args) {
        Game g = new Game(Player.WHITE);
        new GameClient(g);
    }

    public GameClient(Game g) {
        gameClients.add(this);
        createGUI(g);
    }

    public void createGUI(Game g) {
        log.info("Generating GUI");

        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle(g.getName() != null ? WINDOW_TITLE + " - " + g.getName() : WINDOW_TITLE);

        GameClient gc = this;
        this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                gameClients.remove(gc);
                if (gameClients.isEmpty()) {
                    System.exit(0);
                } else {
                    e.getWindow().dispose();
                }
            }
        });

        Container panel = this.getContentPane();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLayeredPane layers = new JLayeredPane();
        layers.setPreferredSize(new Dimension(400,400));
        layers.setBorder(BorderFactory.createLineBorder(Color.magenta));

        Tile[][] tile = g.getTiles();

        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                layers.add(tile[x][y], 0);
            }
        }

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        JLabel timer = new JLabel("Doba hraní: 00:00:00");
        panel.add(timer, c);
        executor.scheduleAtFixedRate(() -> {
            int totalSec = g.addOneSecond();
            int hours = totalSec / 3600;
            int minutes = (totalSec % 3600) / 60;
            int seconds = totalSec % 60;

            timer.setText(String.format("Doba hraní: %02d:%02d:%02d", hours, minutes, seconds));
        }, 0,1, TimeUnit.SECONDS);

        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 1;
        panel.add(layers, c);

        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("Soubor");

        JMenuItem newGame = new JMenuItem("Nová hra");
        fileMenu.add(newGame);
        newGame.addActionListener(event -> {
            Game newGameObject = new Game(Player.WHITE);
            new GameClient(newGameObject);
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });

        JMenuItem saveGame = new JMenuItem("Uložit hru");
        fileMenu.add(saveGame);
        saveGame.addActionListener(new SaveGameEvent(g));

        JMenuItem loadGame = new JMenuItem("Načíst hru");
        fileMenu.add(loadGame);
        loadGame.addActionListener(new LoadGameEvent());

        menuBar.add(fileMenu);
        setJMenuBar(menuBar);
        this.setVisible(true);
    }

}