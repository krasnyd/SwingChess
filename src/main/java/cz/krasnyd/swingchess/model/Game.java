package cz.krasnyd.swingchess.model;

import cz.krasnyd.swingchess.model.pieces.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Game implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger(Game.class);

    private String name;

    private Player activePlayer;
    private Tile activeTile = null;
    private List<Tile> possibleTiles = new ArrayList<>();

    private GameState state = GameState.NONE;

    private final List<King> gameKings = new ArrayList<>();

    private int totalSec = 0;

    private final Tile[][] chess = new Tile[8][8];

    public Game(Player startingPlayer) {
        this.activePlayer = startingPlayer;
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                chess[x][y] = new Tile(x, y, this);
            }
        }

        new Rook(Player.WHITE, chess[0][0]);
        new Rook(Player.BLACK, chess[0][7]);
        new Rook(Player.WHITE, chess[7][0]);
        new Rook(Player.BLACK, chess[7][7]);

        new Knight(Player.WHITE, chess[1][0]);
        new Knight(Player.BLACK, chess[1][7]);
        new Knight(Player.WHITE, chess[6][0]);
        new Knight(Player.BLACK, chess[6][7]);

        new Bishop(Player.WHITE, chess[2][0]);
        new Bishop(Player.BLACK, chess[2][7]);
        new Bishop(Player.WHITE, chess[5][0]);
        new Bishop(Player.BLACK, chess[5][7]);

        new Queen(Player.WHITE, chess[3][0]);
        new Queen(Player.BLACK, chess[3][7]);

        new King(Player.WHITE, chess[4][0]);
        new King(Player.BLACK, chess[4][7]);

        for (int i = 0; i < 8; i++) {
            new Pawn(Player.WHITE, chess[i][1]);
            new Pawn(Player.BLACK, chess[i][6]);
        }


    }

    public void addGameKing(King king) {
        gameKings.add(king);
    }

    public King getPlayerKing() {
        if (gameKings.get(0).getPlayer() == getActivePlayer()) {
            return gameKings.get(0);
        } else {
            return gameKings.get(1);
        }
    }

    public King getEnemyKing() {
        if (gameKings.get(0).getPlayer() != getActivePlayer()) {
            return gameKings.get(0);
        } else {
            return gameKings.get(1);
        }
    }

    public King getKing(Player p) {
        if (gameKings.get(0).getPlayer() == p) {
            return gameKings.get(0);
        } else {
            return gameKings.get(1);
        }
    }

    public Tile getTile(int x, int y) {
        if (x < 0 || x >= 8 || y < 0 || y >= 8) {
            log.debug("Getting tile [{}; {}]: null", x, y);
            return null;
        } else {
            log.debug("Getting tile [{}; {}]: {}", x, y, this.getTiles()[x][y]);
            return this.getTiles()[x][y];
        }
    }

    public Tile getEnemyOrEmptyTile(int x, int y) {
        Tile t = this.getTile(x, y);
        if (t == null) {
            return null;
        } else if (t.getPiece() == null || t.getPiece().getPlayer() != this.activePlayer) {
            return t;
        } else {
            return null;
        }
    }

    public Tile getEmptyTile(int x, int y) {
        Tile t = this.getTile(x, y);
        if (t.getPiece() == null) {
            return t;
        } else {
            return null;
        }
    }

    public Tile getEnemyTile(int x, int y) {
        Tile t = this.getTile(x, y);
        if (t == null || t.getPiece() == null || t.getPiece().getPlayer() == null) {
            return null;
        } else if (t.getPiece().getPlayer() != this.activePlayer) {
            return t;
        } else {
            return null;
        }
    }


    public Tile[][] getTiles() {
        return chess;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(Player activePlayer) {
        this.activePlayer = activePlayer;
    }

    public void swapPlayers() {
        if (activePlayer == Player.BLACK) {
            activePlayer = Player.WHITE;
        } else if (activePlayer == Player.WHITE) {
            activePlayer = Player.BLACK;
        }

        // TODO implement settings for field rotating
//        for (int y = 0; y < 8; y++) {
//            for (int x = 0; x < 8; x++) {
//                chess[x][y].moveToProperPosition();
//            }
//        }
    }

    public void tileClick(Tile t) {
        if (!getState().isGameEnding()) {
            if (this.possibleTiles != null && this.possibleTiles.contains(t)) {
                doMove(t);
            } else if (!t.isEmptyTile() && t.getPiece().getPlayer() == this.activePlayer) {
                activateTile(t);
            }
        }
    }

    public void doMove(Tile t) {
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                chess[x][y].setBackground(null);
            }
        }

        ChessPiece piece = this.activeTile.getPiece();
        this.activeTile.setPiece(null);
        t.setPiece(piece);
        this.setPossibleTiles(null);
        this.activeTile = null;
        t.afterMove();
        swapPlayers();

        boolean playerHasCheck = checkPlayersCheck();
        boolean playerHasPossibleMoves = hasPlayerPossibleMoves();
        if (playerHasCheck && playerHasPossibleMoves) {
            setState(GameState.CHECK);
            log.info("{} dostal šach", getActivePlayer());
        } else if (playerHasCheck && !playerHasPossibleMoves) {
            setState(GameState.CHECKMATE);
            log.info("{} prohrál (šach mat)", getActivePlayer());
            JOptionPane.showMessageDialog(null,
                    "Hráč " + getActivePlayer().getPlayerReverse() + " vyhrál partii!",
                    "Konec hry",
                    JOptionPane.WARNING_MESSAGE);
        } else if (!playerHasPossibleMoves) {
            setState(GameState.STALEMATE);
            log.info("Remíza");
            JOptionPane.showMessageDialog(null,
                    "Hra skončila remízou",
                    "Remíza",
                    JOptionPane.WARNING_MESSAGE);
        }
    }

    public boolean hasPlayerPossibleMoves() {
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                ChessPiece piece = chess[x][y].getPiece();
                if (piece != null
                        && piece.getPlayer() == this.getActivePlayer()
                        && !piece.getFinalPossibleTiles().isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkPlayersCheck() {
        List<Tile> tmp = new ArrayList<>();
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                ChessPiece piece = chess[x][y].getPiece();
                if (piece != null && piece.getPlayer() == getActivePlayer().getPlayerReverse()) {
                    tmp.addAll(piece.getBlockingTiles());
                }
            }
        }
        return tmp.contains(getKing(getActivePlayer()).getTile());
    }

    public void activateTile(Tile t) {
        log.info("Player {} activated tile {}. ", activePlayer, t);
        List<Tile> posTiles = t.getPiece().getFinalPossibleTiles();
        this.activeTile = t;
        this.setPossibleTiles(posTiles);
    }

    public void setPossibleTiles(List<Tile> possibleTiles) {
        if (possibleTiles == null) {
            possibleTiles = new ArrayList<>();
        }
        for (Tile posT : this.possibleTiles) {
            posT.deactivateTile();
        }
        this.possibleTiles = possibleTiles;
        for (Tile posT : this.possibleTiles) {
            posT.activateTile();
        }
    }

    public List<Tile> getPlayerPossibleTiles() {
        List<Tile> tmp = new ArrayList<>();
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                if (chess[x][y].getPiece() != null && chess[x][y].getPiece().getPlayer() != activePlayer) {
                    tmp.addAll(chess[x][y].getPiece().getBlockingTiles());
                }
            }
        }
        tmp.removeAll(Collections.singleton(null));
        return tmp;
    }


    public String generateSaveFileName() {
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy--hh-mm-ss");
        return format.format(now);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public int addOneSecond() {
        return ++totalSec;
    }
}
