package cz.krasnyd.swingchess.model.pieces;

import cz.krasnyd.swingchess.model.Game;
import cz.krasnyd.swingchess.model.Player;
import cz.krasnyd.swingchess.model.Tile;

import java.util.ArrayList;
import java.util.List;

public class King extends ChessPiece {

    private static final long serialVersionUID = 5L;

    public King(Player player, Tile tile) {
        super(player, tile);
        tile.getGame().addGameKing(this);
    }

    @Override
    public List<Tile> getPossibleTiles() {
        List<Tile> posTiles = new ArrayList<>();
        Game g = this.getTile().getGame();
        int x = getTile().getPosX();
        int y = getTile().getPosY();

        posTiles.add(g.getEnemyOrEmptyTile(x + 1, y + 1));
        posTiles.add(g.getEnemyOrEmptyTile(x + 1, y - 1));
        posTiles.add(g.getEnemyOrEmptyTile(x - 1, y + 1));
        posTiles.add(g.getEnemyOrEmptyTile(x - 1, y - 1));
        posTiles.add(g.getEnemyOrEmptyTile(x, y + 1));
        posTiles.add(g.getEnemyOrEmptyTile(x, y - 1));
        posTiles.add(g.getEnemyOrEmptyTile(x + 1, y));
        posTiles.add(g.getEnemyOrEmptyTile(x - 1, y));

//        g.swapPlayers();

        List<Tile> allPostTiles = g.getPlayerPossibleTiles();
        posTiles.removeIf(allPostTiles::contains);

//        g.swapPlayers();

        return posTiles;
    }

    @Override
    public List<Tile> getBlockingTiles() {
        List<Tile> posTiles = new ArrayList<>();
        Game g = this.getTile().getGame();
        int x = getTile().getPosX();
        int y = getTile().getPosY();

        posTiles.add(g.getTile(x + 1, y + 1));
        posTiles.add(g.getTile(x + 1, y - 1));
        posTiles.add(g.getTile(x - 1, y + 1));
        posTiles.add(g.getTile(x - 1, y - 1));
        posTiles.add(g.getTile(x, y + 1));
        posTiles.add(g.getTile(x, y - 1));
        posTiles.add(g.getTile(x + 1, y));
        posTiles.add(g.getTile(x - 1, y));
        return posTiles;
    }

}
