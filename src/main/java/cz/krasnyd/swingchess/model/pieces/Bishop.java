package cz.krasnyd.swingchess.model.pieces;

import cz.krasnyd.swingchess.model.Player;
import cz.krasnyd.swingchess.model.Tile;

import java.util.ArrayList;
import java.util.List;

public class Bishop extends ChessPiece {

    private static final long serialVersionUID = 4L;

    public Bishop(Player player, Tile tile) {
        super(player, tile);
    }

    @Override
    public List<Tile> getPossibleTiles() {
        List<Tile> posTiles = new ArrayList<>();

        posTiles.addAll(helpingMethod(1,1));
        posTiles.addAll(helpingMethod(-1,-1));
        posTiles.addAll(helpingMethod(-1,1));
        posTiles.addAll(helpingMethod(1,-1));

        return posTiles;
    }

}

