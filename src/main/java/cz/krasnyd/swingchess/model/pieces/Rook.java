package cz.krasnyd.swingchess.model.pieces;

import cz.krasnyd.swingchess.model.Player;
import cz.krasnyd.swingchess.model.Tile;

import java.util.ArrayList;
import java.util.List;

public class Rook extends ChessPiece {

    private static final long serialVersionUID = 9L;

    public Rook(Player player, Tile tile) {
        super(player, tile);
    }

    @Override
    public List<Tile> getPossibleTiles() {
        List<Tile> posTiles = new ArrayList<>();

        posTiles.addAll(helpingMethod(1,0));
        posTiles.addAll(helpingMethod(-1,0));
        posTiles.addAll(helpingMethod(0,1));
        posTiles.addAll(helpingMethod(0,-1));

        return posTiles;

    }

}
