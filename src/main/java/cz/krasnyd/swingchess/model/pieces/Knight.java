package cz.krasnyd.swingchess.model.pieces;

import cz.krasnyd.swingchess.model.Game;
import cz.krasnyd.swingchess.model.Player;
import cz.krasnyd.swingchess.model.Tile;

import java.util.ArrayList;
import java.util.List;

public class Knight extends ChessPiece {

    private static final long serialVersionUID = 6L;

    public Knight(Player player, Tile tile) {
        super(player, tile);
    }

    @Override
    public List<Tile> getPossibleTiles() {
        List<Tile> posTiles = new ArrayList<>();
        Game g = this.getTile().getGame();
        int x = getTile().getPosX();
        int y = getTile().getPosY();

        posTiles.add(g.getEnemyOrEmptyTile(x + 1, y + 2));
        posTiles.add(g.getEnemyOrEmptyTile(x + 1, y - 2));
        posTiles.add(g.getEnemyOrEmptyTile(x - 1, y + 2));
        posTiles.add(g.getEnemyOrEmptyTile(x - 1, y - 2));
        posTiles.add(g.getEnemyOrEmptyTile(x + 2, y + 1));
        posTiles.add(g.getEnemyOrEmptyTile(x + 2, y - 1));
        posTiles.add(g.getEnemyOrEmptyTile(x - 2, y + 1));
        posTiles.add(g.getEnemyOrEmptyTile(x - 2, y - 1));
        return posTiles;
    }

}
