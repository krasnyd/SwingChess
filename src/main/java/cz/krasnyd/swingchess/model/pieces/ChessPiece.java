package cz.krasnyd.swingchess.model.pieces;

import cz.krasnyd.swingchess.model.Game;
import cz.krasnyd.swingchess.model.Player;
import cz.krasnyd.swingchess.model.Tile;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ChessPiece implements Serializable {

    private static final long serialVersionUID = 3L;

    protected Player player;
    protected Tile tile;

    public ChessPiece(Player player, Tile tile) {
        this.tile = tile;
        tile.setPiece(this);
        this.player = player;


    }

    public Image getImage() {
        String imageName = "chessPieces/" + this.getClass().getSimpleName().toLowerCase() + "_" + player.getImageName() + ".png";
        URL url = getClass().getClassLoader().getResource(imageName);
        if (url == null) {
            throw new IllegalArgumentException();
        }
        return new ImageIcon(url).getImage();

    }

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public Player getPlayer() {
        return player;
    }

    public abstract List<Tile> getPossibleTiles();
    public List<Tile> getBlockingTiles() {
        return getPossibleTiles();
    }

    public List<Tile> getFinalPossibleTiles() {
        List<Tile> tiles = getPossibleTiles();
        tiles.removeAll(Collections.singleton(null));

        List<Tile> tilesToRemove = new ArrayList<>();

        Tile originalTile = this.getTile();
        ChessPiece chP = this.getTile().getPiece();
        this.getTile().setPiece(null, false);
        for (Tile t : tiles) {
            t.setPiece(chP, false);
            if (getTile().getGame().checkPlayersCheck()) {
                tilesToRemove.add(t);
            }
            t.setPiece(t.getLastPiece());
        }

        tiles.removeIf(tilesToRemove::contains);

        originalTile.setPiece(this.getTile().getLastPiece(), false);

        return tiles;
    }

    protected List<Tile> helpingMethod(int appendX, int appendY) {
        List<Tile> posTiles = new ArrayList<>();
        Game g = this.getTile().getGame();
        int x = getTile().getPosX() + appendX;
        int y = getTile().getPosY() + appendY;
        Player p = this.getPlayer();

        Tile t = g.getTile(x, y);
        while (t != null) {

            x = x + appendX;
            y = y + appendY;

            if (t.isEmptyTile()
                    || (t.getPiece() != null && t.getPiece().getPlayer() == p.getPlayerReverse())) {
                posTiles.add(t);
            }
            if ((t.getPiece() != null && t.getPiece().getPlayer() == p)
                    || (t.getPiece() != null && t.getPiece().getPlayer() == p.getPlayerReverse())) {
                break;
            }

            t = g.getTile(x, y);

        }
        return posTiles;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "-" + player;
    }
}
