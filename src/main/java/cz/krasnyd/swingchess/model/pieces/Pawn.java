package cz.krasnyd.swingchess.model.pieces;

import cz.krasnyd.swingchess.model.Game;
import cz.krasnyd.swingchess.model.Player;
import cz.krasnyd.swingchess.model.Tile;

import java.util.ArrayList;
import java.util.List;

public class Pawn extends ChessPiece {

    private static final long serialVersionUID = 7L;

    public Pawn(Player player, Tile tile) {
        super(player, tile);
    }

    @Override
    public List<Tile> getPossibleTiles() {
        List<Tile> posTiles = new ArrayList<>();
        Game g = this.getTile().getGame();
        int x = getTile().getPosX();
        int y = getTile().getPosY();

        if (player == Player.BLACK) {
            Tile firstTile = g.getEmptyTile(x, y - 1);
            posTiles.add(firstTile);
            if (y == 6 && firstTile != null && firstTile.isEmptyTile()) {
                posTiles.add(g.getEmptyTile(x, y - 2));
            }

            posTiles.add(g.getEnemyTile(x - 1, y - 1));
            posTiles.add(g.getEnemyTile(x + 1, y - 1));
        } else {
            Tile firstTile = g.getEmptyTile(x, y + 1);
            posTiles.add(firstTile);
            if (y == 1 && firstTile != null && firstTile.isEmptyTile()) {
                posTiles.add(g.getEmptyTile(x, y + 2));
            }

            posTiles.add(g.getEnemyTile(x + 1, y + 1));
            posTiles.add(g.getEnemyTile(x - 1, y + 1));
        }
        return posTiles;
    }
    @Override
    public List<Tile> getBlockingTiles() {
        List<Tile> posTiles = new ArrayList<>();
        Game g = this.getTile().getGame();
        int x = getTile().getPosX();
        int y = getTile().getPosY();

        if (player == Player.BLACK) {
            posTiles.add(g.getTile(x - 1, y - 1));
            posTiles.add(g.getTile(x + 1, y - 1));
        } else {
            posTiles.add(g.getTile(x + 1, y + 1));
            posTiles.add(g.getTile(x - 1, y + 1));
        }
        return posTiles;
    }

}
