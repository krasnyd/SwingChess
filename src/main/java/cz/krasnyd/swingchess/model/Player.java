package cz.krasnyd.swingchess.model;

public enum Player {
    WHITE {
        @Override
        public Player getPlayerReverse() {
            return BLACK;
        }
    },
    BLACK {
        @Override
        public Player getPlayerReverse() {
            return WHITE;
        }
    };

    public Player getPlayerReverse() {
        return null;
    }

    public String getImageName() {
        return this.toString().toLowerCase();
    }
}
