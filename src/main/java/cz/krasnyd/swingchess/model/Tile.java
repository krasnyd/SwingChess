package cz.krasnyd.swingchess.model;

import cz.krasnyd.swingchess.model.pieces.ChessPiece;
import cz.krasnyd.swingchess.model.pieces.Pawn;
import cz.krasnyd.swingchess.model.pieces.Queen;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;


public class Tile extends JPanel implements MouseListener, Serializable {

    private static final long serialVersionUID = 2L;

    private static final Logger log = LogManager.getLogger(Tile.class);

    private final int x;
    private final int y;
    private Color background = null;

    private static final int TILE_WIDTH = 50;
    private static final int TILE_HEIGHT = 50;

    private Game game;

    private ChessPiece piece = null;
    private ChessPiece lastPiece = null;

    private boolean active = false;

    public Tile(int x, int y, Game game) {
        this.x = x;
        this.y = y;
        this.game = game;

        addMouseListener(this);
        moveToProperPosition();
    }

    public void moveToProperPosition() {
        setBounds(getVisibleX(), getVisibleY(), TILE_WIDTH, TILE_HEIGHT);
    }

    private int getVisibleX() {
        return x * TILE_WIDTH;
    }

    private int getVisibleY() {
        if (game.getActivePlayer() == Player.WHITE) {
            return 400 - (y + 1) * TILE_HEIGHT;
        } else if(game.getActivePlayer() == Player.BLACK) {
            return y * TILE_HEIGHT;
        }
        return 0;
    }

    @Override
    public Color getBackground() {
        boolean odd = (y % 2 == 0) ? (x % 2 == 0) : (x % 2 == 1);
        if (background != null) {
            return background;
        } else if (odd) {
            return new Color(209,139,71);
        } else {
            return new Color(255,206,158);
        }
    }

    @Override
    public void setBackground(Color background) {
        this.background = background;
        repaint();
    }

    public Game getGame() {
        return game;
    }

    public void setPiece(ChessPiece piece, boolean repaint) {
        if (piece != null) {
            piece.setTile(this);
        }
        this.lastPiece = this.piece;
        this.piece = piece;
        if (repaint) {
            repaint();
        }
    }

    public void setPiece(ChessPiece piece) {
        setPiece(piece, true);
    }

    public ChessPiece getPiece() {
        return piece;
    }

    public boolean isEnemyTile() {
        if (this.getPiece() == null) {
            return false;
        }
        return this.getPiece().getPlayer() != this.getGame().getActivePlayer();
    }

    public boolean isEmptyTile() {
        return this.getPiece() == null;
    }

    public boolean isPlayerTile() {
        if (this.getPiece() == null) {
            return false;
        }
        return this.getPiece().getPlayer() == this.getGame().getActivePlayer();
    }

    @Override
    public String toString() {
        return "Tile{" +
                "x=" + x +
                ", y=" + y +
                ", VisibleX=" + getVisibleX() +
                ", VisibleY=" + getVisibleY() +
                ", piece=" + getPiece() +
                '}';
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (piece != null) {
            g.drawImage(piece.getImage(), 0, 0, 50, 50, null);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // Not implemented
    }

    @Override
    public void mousePressed(MouseEvent event) {
        try {
            game.tileClick(this);
        } catch (Exception e) {
            log.error("Tile event error. ", e);
            JOptionPane.showMessageDialog(this.getParent(),
                    "Nepodařilo se provést tuto operaci. Chyba: \n" + e.getClass().getName() + ": " + e.getLocalizedMessage(),
                    "Chyba",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Not implemented
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if ((this.isPlayerTile() || active) && !getGame().getState().isGameEnding()) {
            setCursor(new Cursor(Cursor.HAND_CURSOR));
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    public int getPosX() {
        return x;
    }

    public int getPosY() {
        return y;
    }

    public void deactivateTile() {
        this.setBackground(null);
        this.active = false;
    }

    public void activateTile() {
        this.setBackground(Color.red);
        this.active = true;
    }

    public ChessPiece getLastPiece() {
        return lastPiece;
    }

    public void setLastPiece(ChessPiece lastPiece) {
        this.lastPiece = lastPiece;
    }

    public void afterMove() {
        if (this.getPiece() instanceof Pawn) {
            if (this.getPiece().getPlayer() == Player.BLACK && this.getPosY() == 0) {
                new Queen(Player.BLACK, this);
            } else if (this.getPiece().getPlayer() == Player.WHITE && this.getPosY() == 7) {
                new Queen(Player.WHITE, this);
            }
        }
    }
}
