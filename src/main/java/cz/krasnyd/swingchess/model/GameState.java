package cz.krasnyd.swingchess.model;

public enum GameState {
    NONE(false),
    CHECK(false),
    CHECKMATE(true),
    STALEMATE(true);

    private final boolean gameEnding;
    GameState(boolean gameEnding) {
        this.gameEnding = gameEnding;
    }

    public boolean isGameEnding() {
        return gameEnding;
    }
}
