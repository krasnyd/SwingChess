package cz.krasnyd.swingchess;

import cz.krasnyd.swingchess.model.Game;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class LoadGameEvent implements ActionListener {

    private static final Logger log = LogManager.getLogger(LoadGameEvent.class);

    @Override
    public void actionPerformed(ActionEvent event) {
        FileDialog fd = new FileDialog((Dialog) null, "Uložit hru", FileDialog.LOAD);
        fd.setVisible(true);

        String filePath = fd.getDirectory() + fd.getFile();
        if (fd.getFile() != null) {
            try (FileInputStream fi = new FileInputStream(new File(filePath));
                 ObjectInputStream oi = new ObjectInputStream(fi)) {

                // read object from file
                Game loadedGame = (Game) oi.readObject();
                loadedGame.setName(FilenameUtils.removeExtension(fd.getFile()));

                // open new game
                new GameClient(loadedGame);

            } catch (IOException | ClassNotFoundException e) {
                log.error("Error while loading game. ", e);
                JOptionPane.showMessageDialog(null,
                        "Nepodařilo se načíst uloženou hru. Chyba: \n" + e.getClass().getName() + ": " + e.getLocalizedMessage(),
                        "Chyba",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
